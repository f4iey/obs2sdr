@echo off
rem OBS to IP Stream
rem By Dominique F1EJP creation of the script inspired by the dave script G8GKQ 
rem V01.15

:START
rem Set coding NVIDIA GPU=1 or Processor INTEL GPU=2
rem Entrez Codage coding NVIDIA GPU=1 or Processor INTEL GPU=2
rem ----------------------------------------------------------
set GPU=1

rem Set the IP address of your Transmiter here
rem Entrez IP de l'emetteur et port
rem ------------------------------------------
set ip=192.168.0.30:10000
rem for test with VLC 
rem set ip=230.0.0.10:10000

rem Set your callsign here
rem Entrez votre indicatif
rem ----------------------
set callsign=F1EJP

rem Default frame rate - Frequence trame par defaut
rem -----------------------------------------------
set Fi=25

rem Default value for image size and audio
rem --------------------------------------
set IMAGESIZE=1920x1080
set AUDIO=128000

rem Default Correction coefficient for low flow compression
rem Coefficient correcteur par défaut pour compression bas débit
rem ------------------------------------------------------------
set COEF=0

rem Print banner
cls
echo:
echo:
echo H264 and H.265 Video Encoder with FFMPEG and NVIDIA or INTEL
echo H264 and H.265 Encodeur Video avec FFMPEG et NVIDIA ou INTEL
echo =============================================================
echo:
echo Transmiter IP: %ip%  Your callsign set to %callsign%
echo:
echo Start this script once OBS is running with Virtual CAM installed
echo then leave it running.  Then select Transmit on the transmitter
echo:
echo This script is for SRs of 25KS 35KS 66KS, 125KS, 250kS, 333kS, 500kS, 1000KS and 2000kS
echo Set the Modulation type, SR, FEC and coding headroom
echo Press Enter for default (previous) values
echo:

SETLOCAL EnableExtensions
set EXE=obs64.exe
FOR /F %%x IN ('tasklist /NH /FI "IMAGENAME eq %EXE%"') DO IF %%x == %EXE% goto :requestMod
echo:
echo ---------------OBS is not START---------------
echo ---------------OBS n'est pas demarre-------------
echo:
pause
goto :START

:requestMod
if X%MOD%==X set MOD=2
set defMOD=%MOD%
set /p MOD=Enter Modulation (1 for DVB-S, 2 DVB-S2 QPSK, 3 DVB-S2 8PSK, 4 DVB-S2 16PSK, 5 DVB-S2 32PSK) (%MOD%):
set _tempvar=0
if %MOD%==1  set _tempvar=1
if %MOD%==2  set _tempvar=1
if %MOD%==3  set _tempvar=1
if %MOD%==4  set _tempvar=1
if %MOD%==5  set _tempvar=1
if NOT %_tempvar% EQU 1 goto :requestMod
echo:

:requestCod
if X%COD%==X set COD=2
set defCOD=%COD%
set /p COD=Enter Codec (1 for H264, 2 for H265) (%COD%):
set _tempvar=0
if %COD%==1  set _tempvar=1
if %COD%==2  set _tempvar=1
if %COD%==1  set CODEC=H264
if %COD%==2  set CODEC=H265
if %GPU%==1  if %COD%==1 set COMP=h264_nvenc
if %GPU%==1  if %COD%==2 set COMP=hevc_nvenc
if %GPU%==2  if %COD%==1 set COMP=h264_qsv
if %GPU%==2  if %COD%==2 set COMP=hevc_qsv
if NOT %_tempvar% EQU 1 goto :requestCod
echo:

:requestSR
if X%SR%==X set SR=333
set defSR=%SR%
set /p SR=Enter SR (25, 35, 66, 125, 250, 333, 500, 1000 or 2000) in kS (%SR%):

set _tempvar=0
if %SR%==25  set _tempvar=1
if %SR%==35  set _tempvar=1
if %SR%==66  set _tempvar=1
if %SR%==125  set _tempvar=1
if %SR%==250  set _tempvar=1
if %SR%==333  set _tempvar=1
if %SR%==500  set _tempvar=1
if %SR%==1000 set _tempvar=1
if %SR%==2000 set _tempvar=1
if NOT %_tempvar% EQU 1 goto :requestSR

if X%SR%==X set SR=%defSR%
echo:

:requestFEC

rem Available on portsdown--1/4, 1/3, 1/2, 3/5, 2/3, 3/4, 5/6, 7/8, 8/9, 9/10

if %MOD%==1 (
echo Standard DVBS---------------------1/2, 2/3, 3/4, 5/6, 7/8
echo :	
echo Available FECs on portsdown are   1/2, 2/3, 3/4, 5/6, 7/8
)
if %MOD%==2 (
echo standard DVBS2 -------------------1/4, 1/3, 2/5, 1/2, 3/5, 2/3, 3/4, 4/5, 5/6, 8/9, 9/10 
echo:
echo Available FECs on portsdown are   1/4, 1/3, 1/2, 3/5, 2/3, 3/4, 5/6, 8/9, 9/10
)
if %MOD%==3 (
echo standard DVBS2 8PSK---------------3/5, 2/3, 3/4, 5/6, 8/9, 9/10 
echo:
echo Available FECs on portsdown are   3/5, 2/3, 3/4, 5/6, 8/9, 9/10
)
if %MOD%==4 (
echo standard DVBS2 16PSK--------------2/3, 3/4, 5/6, 8/9, 9/10 
echo:
echo Available FECs on portsdown are   2/3, 3/4, 5/6, 8/9, 9/10
)
if %MOD%==5 (
echo standard DVBS2 32PSK--------------3/4, 5/6, 8/9, 9/10 
echo:
echo Available FECs on portsdown are   3/4, 5/6, 8/9, 9/10
)
echo:

if X%FEC%==X set FEC=1/2
set defFEC=%FEC%
set /p FEC=Enter FEC (%FEC%):

rem tBCH parametre DVBS2 and verification choice
set tBCH=0
if %FEC%==1/4 set tBCH=12
if %FEC%==1/3 set tBCH=12
if %FEC%==1/2 set tBCH=12
if %FEC%==3/5 set tBCH=12
if %FEC%==2/3 set tBCH=10
if %FEC%==3/4 set tBCH=12
if %FEC%==5/6 set tBCH=10
if %FEC%==7/8 set tBCH=8
if %FEC%==8/9 set tBCH=8
if %FEC%==9/10 set tBCH=8
if NOT %tBCH% GTR 1 goto :requestFEC

if X%FEC%==X set FEC=%defFEC%
echo:

:requestHEADROOM
if X%HEADROOM%==X set HEADROOM=35
set defHEADROOM=%HEADROOM%
set /p HEADROOM=Enter Coding Headroom (0 to 100 percent, 35 suggested for DVBS, 25 for DVBS2) (%HEADROOM%):

rem Save values for next time
rem If these lines cause errors your system may not support SETX
rem If so just comment them out (use rem)

if not %MOD%==%defMOD% setx MOD "%MOD%"
if not %COD%==%defCOD% setx COD "%COD%"
if not %SR%==%defSR% setx SR "%SR%"
if not %FEC%==%defFEC% setx FEC "%FEC%"
if not %HEADROOM%==%defHEADROOM% setx HEADROOM "%HEADROOM%"

rem ----------------------------------------------------------------------------------------------------

rem Calculation of the parameters according to the choices made

rem CALCULATION DEBIT DVB-S (with round)
rem =======================
set /a VIDRATEdvbs=(((%SR%*200*%FEC%*188/204)+5)/10)*100

rem CALCULATION DEBIT formula DVB-S2
rem ================================
rem Net_data_rate = symbol_rate / (FEC_frame/q + 90 + ceil((FEC_frame/q/90/16-1))*36)*(FEC_frame*code_rate-(16*tBCH)-80)
rem FEC_frame = 64800 or 16200 bit
rem 90 = symbols PL header
rem q = 2,3,4,5 bit/symbol (QPSK,8PSK,16APSK,32APSK)
rem ceil(A) rounds A ..arrondi à integré plus grand ou égal à A
rem 36 = pilot symbols > if not = 0 (facultatif pilotes de synchronisation)
rem code_rate (FEC) = 1/4....9/10
rem tbch 8 10 ou 12
rem 80 = bit DF header

rem Simplified FLOW CALCULATION DVB-S2 without pilot symbol and FEC Frame 64800
rem ==================================
rem Net_data_rate = symbol_rate / (FEC_frame/q + 90 + 0) * (FEC_frame*code_rate-(16*tBCH)-80)
rem multiplied by 10,000 and divided by 10 to increase accuracy (comma-free calculation with batch)
rem ==================================
set /a VIDRATEdvbs2a=(%SR%*((((64800*%FEC%)-(16*%tBCH%)-80)*10000)/((64800/%MOD%)+90)))/10
rem round
set /a VIDRATEdvbs2b=%VIDRATEdvbs2a%/100
set /a VIDRATEdvbs2=%VIDRATEdvbs2b%*100

if %MOD%==1 goto DVBS1
if %MOD%==2 goto DVBS2
if %MOD%==3 goto DVBS2
if %MOD%==4 goto DVBS2
if %MOD%==5 goto DVBS2
:DVBS1
(
  rem DVB-S Flow, image size, sound
    if %SR%==25 (
    set IMAGESIZE=426x240
    set AUDIO=8000
    set Fi=15
  )
  if %SR%==35 (
    set IMAGESIZE=426x240
    set AUDIO=8000
    set Fi=15
  )
if %SR%==66 (
    set IMAGESIZE=640x360
    set AUDIO=12000
    set Fi=15
  )
  if %SR%==125 (
    set IMAGESIZE=640x360
    set AUDIO=16000
  )
  if %SR%==250 (
    set IMAGESIZE=960x540
    set AUDIO=32000
  )
  if %SR%==333 (
    set IMAGESIZE=1280x720
    set AUDIO=64000
  )
  if %SR%==500 (
    set IMAGESIZE=1280x720
    set AUDIO=64000
  )
  set VIDRATE=%VIDRATEdvbs%
)
goto SUITE1

:DVBS2 
(
  rem DVB-S2 Flow, image size, sound
    if %SR%==25 (
    set IMAGESIZE=426x240
    set AUDIO=8000
    set Fi=15
  )
  if %SR%==35 (
    set IMAGESIZE=426x240
    set AUDIO=8000
    set Fi=15
  )
  if %SR%==66 (
    set IMAGESIZE=640x360
    set AUDIO=12000
    set Fi=15
  )
  if %SR%==125 (
    set IMAGESIZE=640x360
    set AUDIO=16000
  )
  if %SR%==250 (
    set IMAGESIZE=960x540
    set AUDIO=32000
  )
  if %SR%==333 (
    set IMAGESIZE=1280x720
    set AUDIO=64000
  )
  if %SR%==500 (
    set AUDIO=64000
  )
  set VIDRATE=%VIDRATEdvbs2%
)
goto SUITE1

:SUITE1	
set BASEVIDRATE=%VIDRATE%
set /a BASEVIDRATEK=%BASEVIDRATE%/1000
set /a AUDIO1=%AUDIO%*1

rem take account of audio and then factor in headroom
rem =================================================
set /a VIDRATE=(%BASEVIDRATE%-%AUDIO1%)*100/(100+%HEADROOM%)
echo:

Rem change image size for test
rem ==========================
if X%IMAGESIZE%==X set IMAGESIZE=1920x1080
set defIMAGESIZE=%IMAGESIZE%
set /p IMAGESIZE=You can change IMAGESIZE (1920x1080,1280x720,960x540,640x360,426x240 or other) default is (%IMAGESIZE%):

rem set /a BUFSIZE=%BASEVIDRATE%/2
set /a BUFSIZE=%VIDRATE%*1
set /a VIDRATEK=%VIDRATE%/1000
cls
echo:
echo:
echo:
echo:
echo ____________________________________________________________________________________
echo:
if %MOD%==1 goto Netto1
if %MOD%==2 goto Netto2
if %MOD%==3 goto Netto3
if %MOD%==3 goto Netto4
if %MOD%==3 goto Netto5
:Netto1
(
echo 			For DVB-S at %SR% kS and FEC %FEC%
)
goto SUITE2
:Netto2 
(
echo 			For DVB-S2 QPSK at %SR% kS and FEC %FEC% 
	)
goto SUITE2
:Netto3 
(
echo 			For DVB-S2 8PSK at %SR% kS and FEC %FEC% 
	)
goto SUITE2
:Netto4 
(
echo 			For DVB-S2 16PSK at %SR% kS and FEC %FEC% 
	)
goto SUITE2
:Netto5 
(
echo 			For DVB-S2 32PSK at %SR% kS and FEC %FEC% 
	)
goto SUITE2

:SUITE2

echo 			Netto data rate: %BASEVIDRATE% b/s or %BASEVIDRATEK% kb/s
echo:
echo 			With a headroom of %HEADROOM%
echo 			video bitrate set is %VIDRATE% b/s or %VIDRATEK% kb/s
echo:
echo 			Codec: %CODEC%
echo 			Size of video: %IMAGESIZE%
echo 			Frame rate: %FI%
echo:
echo 			Audio birate: %AUDIO% b/s
echo:
echo ____________________________________________________________________________________
echo:

rem Ajout pour Tables et Compression Graphic Card (utile pour très bas débit <= 250k)

set /a COEF=38-((%VIDRATE%/1000)*6/100)
if %COEF% GTR 10 (set COEF1=%COEF%) else set COEF1=0
echo:
set /a VIDRATE=%VIDRATE%-(%COEF1%*1000)
set /a VIDRATEK=%VIDRATE%/1000
rem echo  %COEF1%          Compression FFMPEG " %COMP% " at %VIDRATE% b/s or %VIDRATEK% kb/s
echo:

if %GPU%==1 if %VIDRATEK% GTR 16 goto :CODAGE
if %GPU%==2 if %VIDRATEK% GTR 20 goto :CODAGE 
cls
echo:
echo:
echo:
echo  ----------------------------%BASEVIDRATEK% kb/s---------------------------------
echo:
echo  ---------------------Value too low! start again---------------------------
echo  -------------------Valeur trop faible ! recommencez-----------------------
echo:
echo:
echo:
pause
cls
goto :START

:CODAGE
pause

if %GPU%==1 (

start "Video Transport Stream to Portsdown" /high /min ^
C:\ffmpeg\bin\ffmpeg ^
-rtbufsize 400M ^
-f dshow -thread_queue_size 2048 -i video="OBS-Camera" -f dshow -thread_queue_size 2048 -i audio="OBS-Audio" ^
-c:v %COMP% -s %IMAGESIZE% -r %Fi% -bf 0 -profile:v main -pix_fmt yuv420p -preset slow -rc cbr_hq -aspect 16:9 ^
-b:v %VIDRATE% -minrate %VIDRATE% -maxrate %VIDRATE% -bufsize %BUFSIZE% ^
-c:a aac -ac 1 -b:a %AUDIO% -ar 44100 ^
-f mpegts -mpegts_original_network_id 1 -mpegts_transport_stream_id 1 -mpegts_service_id 1 -mpegts_pmt_start_pid 4096 -streamid 0:256 -streamid 1:257 ^
-metadata service_provider="EJP-%callsign%" -metadata service_name=%callsign% ^
-flush_packets 0 -y ^
-f mpegts "udp://%ip%?pkt_size=1316&bitrate=%BASEVIDRATE%"

)

if %GPU%==2 (

start "Video Transport Stream to Portsdown" /high /min ^
C:\ffmpeg\bin\ffmpeg ^
-rtbufsize 400M ^
-f dshow -thread_queue_size 2048 -i video="OBS-Camera" -f dshow -thread_queue_size 2048 -i audio="OBS-Audio" ^
-c:v %COMP% -s %IMAGESIZE% -r %Fi% -bf 0 -profile:v main -pix_fmt nv12 -preset slow -aspect 16:9 ^
-b:v %VIDRATE% -minrate %VIDRATE% -maxrate %VIDRATE% -bufsize %BUFSIZE% ^
-c:a aac -ac 1 -b:a %AUDIO% -ar 44100 ^
-f mpegts -mpegts_original_network_id 1 -mpegts_transport_stream_id 1 -mpegts_service_id 1 -mpegts_pmt_start_pid 4096 -streamid 0:256 -streamid 1:257 ^
-metadata service_provider="EJP-%callsign%" -metadata service_name=%callsign% ^
-flush_packets 0 -y ^
-f mpegts "udp://%ip%?pkt_size=1316&bitrate=%BASEVIDRATE%"

)

echo:
echo:
echo ------------------------------- START TRANSMISSION ---------------------------------
echo:
if X%FUNCT%==X set FUNCT=0
set defFUNCT=%FUNCT%
set /p FUNCT=Q for Quit Enter for Restart :

if %FUNCT%==Q goto END
if %FUNCT%==q goto END
taskkill /F /IM ffmpeg.exe

goto START
:END
taskkill /F /IM ffmpeg.exe

