# OBS to IP Stream using ffmpeg
# By F1EJP, forked in python by F4IEY
import os
import subprocess
import keyboard
import psutil

# Setting global variables and config

# set your encoding device here
# GPU = 1; for NVIDIA
# GPU = 2; for integrated
GPU = "1"

# set transmitter IP address and port
ip = "192.168.0.30:10000"
# for multicast with MPV
# ip = "230.0.0.10:10000"

# set your callsign here
callsign = "F1EJP"

# default frame rate
Fi = "60"

# default image and audio resolution
IMAGESIZE = "1920x1080"
AUDIO = "128000"

# Default Correction ratio for low compression
COEF = 0

# Print CLI
print("""
H.264/265 video encoder\n
        _         ____          _      
   ___ | |__  ___|___ \ ___  __| |_ __ 
  / _ \| '_ \/ __| __) / __|/ _` | '__|
 | (_) | |_) \__ \/ __/\__ \ (_| | |   
  \___/|_.__/|___/_____|___/\__,_|_|   
                                       
""")

# Check if OBS Studio is running
if not is_obs_running: 
    print("OBS needs to be running!\n")
    quit();

# Modulation
MOD = int(input("Enter Modulation:\n(1) for DVB-S\n(2) DVB-S2 QPSK\n(3) DVB-S2 8PSK\n(4) DVB-S2 16PSK\n(5) DVB-S2 32PSK\n")) or 2

# Codec
COD = int(input("Enter codec:\n(1) for H.264\n(2) for H.265")) or 2
CODEC = "H.264" if COD == 1 else "H.265"
if COD == 1 and GPU == "1": COMP = "h264_nvenc"
elif COD == 2 and GPU == "1": COMP = "hevc_nvenc"
elif COD == 1 and GPU == "2": COMP = "h264_qsv"
elif COD == 2 and GPU == "2": COMP = "hevc_qsv"

# Sample Rate
SR = int(input("Enter SR (is kS):\n25\n35\n66\n125\n250\n(333)\n500\n1000 or 2000\n")) or 333
# FEC
# Available on Portsdown--1/4, 1/3, 1/2, 3/5, 2/3, 3/4, 5/6, 7/8, 8/9, 9/10
# match structure works from v3.10 and higher
match COD:
    case 1:
        print("Standard DVB-S: 1/2, 2/3, 3/4, 5/6, 7/8\n")
    case 2:
        print("Standard DVBS2: 1/4, 1/3, 2/5, 1/2, 3/5, 2/3, 3/4, 5/6, 8/9, 9/10\nOn portsdown: 1/4, 1/3, 1/2, 3/5, 2/3, 3/4, 5/6, 8/9, 9/10\n")
    case 3:
        print("Standard DVBS2 8PSK: 3/5, 2/3, 3/4, 5/6, 8/9, 9/10\n")
    case 4:
        print("Standard DVBS2 16PSK: 2/3, 3/4, 5/6, 8/9, 9/10\n")
    case 5:
        print("Standard DVBS2 32PSK: 3/4, 5/6, 8/9, 9/10\n")
    case _:
        print("ERROR: Modulation not found!\n")

fraction_str = input("Enter FEC : \n") or "1/2"
num, den = map(float, fraction_str.split('/'))
FEC = num / den

# tBCH param DVBS2 and verification
tBCH = 0
if FEC == 1/4: tBCH = 12
elif FEC == 1/3: tBCH = 12
elif FEC == 1/2: tBCH = 12
elif FEC == 3/5: tBCH = 12
elif FEC == 2/3: tBCH = 10
elif FEC == 3/4: tBCH = 12
elif FEC == 5/6: tBCH = 10
elif FEC == 7/8: tBCH = 8
elif FEC == 8/9: tBCH = 8
elif FEC == 9/10: tBCH = 8
else: tBCH = 12

# Headroom
HEADROOM = int(input("Enter Coding Headroom (0 to 100, suggested 35 for DVBS and 25 for DVBS2):\n")) or 35

# Video Rate
VIDRATEdvbs = (((SR*200*FEC*188/204)+5)/10)*100
VIDRATEdvbs2a = (SR*((((64800*FEC)-(16*tBCH)-80)*10000)/((64800/MOD)+90)))/10
VIDRATEdvbs2b = VIDRATEdvbs2a // 100
VIDRATEdbvs2 = VIDRATEdvbs2b * 100


if MOD == 1: dvbs(SR, IMAGESIZE, AUDIO, Fi, VIDRATEdvbs)
else: dvbs(SR, IMAGESIZE, AUDIO, Fi, VIDRATEdbvs2)
os.system("cls") or os.system("clear")

BASEVIDRATE = VIDRATE
BASEVIDRATEK = BASEVIDRATE // 1000
AUDIO1 = AUDIO * 1

# take account of audio and then factor in headroom
VIDRATE = (BASEVIDRATE - AUDIO1)*100 // (100+HEADROOM)

# Change image size for test
IMAGESIZE = input("You can change IMAGESIZE (1920x1080,1280x720,960x540,640x360,426x240 or other) default is (%IMAGESIZE%)\n") or "1920x1080"
# BUFSIZE = BASEVIDRATE // 2
BUFSIZE = VIDRATE * 1
VIDRATEK = VIDRATE // 1000
os.system("clear") or os.system("cls")
print("\n\n")
match MOD:
    case 1:
        print("For DVB-S at ", SR, "kS and FEC ", FEC, "\n")
    case 2:
        print("For DVB-S2 QPSK at ",SR, "kS and FEC ",FEC,"\n")
    case 3:
        print("For DVB-S2 8PSK at ",SR, "kS and FEC ",FEC,"\n")
    case 4:
        print("For DVB-S2 16PSK at ",SR, "kS and FEC ",FEC,"\n")
    case 5:
        print("For DVB-S2 32PSK at ",SR, "kS and FEC ",FEC,"\n")

print("""Netto data rate: """, BASEVIDRATE,""" b/s or """,BASEVIDRATEK,""" kb/s

With a headroom of """,HEADROOM,"""
video bitrate is set to """,VIDRATE,""" b/s or """,VIDRATEK,""" kb/s

Codec: """,CODEC, """
Size of video: """,IMAGESIZE,"""
Frame rate: """,Fi,"""

Audio bitrate: """,AUDIO,""" b/s

-------------------------------------------------------------------

""")
# Ajout pour tables de compression GPU
COEF = 38-((VIDRATE // 1000) * 6 // 100)
if COEF > 10: COEF1 = COEF
else: COEF1 = 0
print("")
VIDRATE -= COEF1*1000
VIDRATEK = VIDRATE // 1000
# Compression FFMPEG
if GPU == "1" and VIDRATEK < 16 or GPU == "2" and VIDRATEK < 20:
    print (BASEVIDRATEK," value is too low! Start over!\n")
    quit()
else:
    codage(GPU, COMP, IMAGESIZE, VIDRATE, BUFSIZE, AUDIO, BASEVIDRATE, Fi, callsign)

def is_obs_running():
    for proc in psutil.process_iter(['name']):
        if proc.info['name'] == 'obs':
            return True
    return False

# Calculation debit DVBS
def dvbs(sr, imgsize, audio, fi, vidRateDVB):
    # DVB-S Flow, image size, sound
    match sr:
        case 25 | 35: 
            imgsize = "426x240"
            audio = "8000"
            fi = "15"
        case 66: 
            imgsize = "640x360"
            audio = "12000"
            fi = "15"
        case 125: 
            imgsize = "640x360"
            audio = "16000"
        case 250: 
            imgsize = "960x540"
            audio = "32000"
        case 333 | 500: 
            imgsize = "1280x720"
            audio = "64000"
    VIDRATE = vidRateDVB

def codage(gpu, comp, imgsize, vidrate, bufsize, audio, basevidrate, Fi, callsign):
    print("Start video stream to Portsdown...\n")
    if gpu == "1": 
        gpuFMT = "nv12"
        gpuRC = ''
        gpuCBRHQ = ''
    else:
        gpuFMT = "yuv420p"
        gpuRC = '-rc'
        gpuCBRHQ = 'cbr_hq'

    subprocess.Popen(['ffmpeg', '-rtbufsize', '400M', 
    '-f', 'dshow', 
    '-thread_queue_size', '2048', 
    '-i', 'video="OBS-Camera"',
    '-f', 'dshow',
    '-thread_queue_size', '2048',
    '-i', 'audio="OBS-Audio"',
    '-c:v', comp,
    '-s', imgsize,
    '-r', Fi,
    '-bf', '0',
    '-profile:v', 'main',
    '-pix_fmt', gpuFMT,
    '-preset', 'slow', gpuRC, gpuCBRHQ,
    '-aspect', '16:9', 
    '-b:v', vidrate,
    '-minrate', vidrate,
    '-maxrate', vidrate,
    '-bufsize', bufsize,
    '-c:a', 'aac',
    '-ac', '1',
    '-b:a', audio, '-ar', '44100',
    '-f', 'mpegts', '-mpegts_original_network_id', '1', '-mpegts_transport_stream_id', '1', '-mpegts_service_id', '1', '-mpegts_pmt_start_pid', '4096', '-streamid', '0:256', '-streamid', '1:257',
    '-metadata', 'service_provider="EJP-' + callsign + '"', '-metadata', 'service_name="' + callsign + '"',
    '-flush_packets', '0', '-y',
    '-f', 'mpegts', f'"udp://{ip}?pkt_size=1316&bitrate={basevidrate}"'])

    print("""
    ------------------------------- START TRANSMISSION ---------------------------------
    """)
    # Attacher la fonction de rappel à l'événement "keypress"
    keyboard.on_press(on_key_press)
    print("\nPress q to kill ffmpeg process...")


# Fonction qui sera appelée lorsque la touche 'q' sera pressée
def on_key_press(key):
    if key.name == 'q' or key.name == "Q":
        keyboard.unhook_all()  # Arrêter la lecture des touches
        os.system("killall ffmpeg") or os.system("taskkill /F /IM ffmpeg.exe")
        quit()  # Quitter le programme
